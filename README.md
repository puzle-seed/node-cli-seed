# Node Cli Seed

## About

Node CLI Seed is a starter project to create a Typscript CLI app.

## Features

- Typescript
- Linter
- Unit testing and coverage with Jest
- CI with gitlab 

## Copy / Clone

```sh
$git clone https://gitlab.com/puzle-seed/node-cli-seed.git
```

## Installation

```sh
$npm install
```

## Test

```sh
$npm run test
```

## Coverage

```sh
$npm run test:coverage
```

## Lint

```sh
$npm run lint
```

## Build

```sh
$npm run build
```

