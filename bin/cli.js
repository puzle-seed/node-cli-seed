#!/usr/bin/env node

'use strict';

const commander = require('commander');
const config = require('../package.json');
const Core = require('../dist/main').Core;
commander
    .version(config.version)
    .option('-o, --option <item>', 'Describe here your commander option')
    .parse(process.argv);
